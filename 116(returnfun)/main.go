package main

import (
	"fmt"
)


func test(n int) int {
	if n==1 || n==2{
		return 1
	}else{
		return test(n-1)+test(n-2)
	}

}


func main(){
	for i:=1; i<10; i++{
		if i==1 || i==2{
			fmt.Println(1) 
		}else{
			//fmt.Println(i) 
			fmt.Println(test(i))
		}
	}
}