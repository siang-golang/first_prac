package main

import "fmt"

var (
	//全局匿名函數
	Fn=func (n1 int,n2 int) int{
		return n1+n2
	}
)

func sum (n1 int, args...int)  int{
	sum:=n1
	for i:=0; i<len(args); i++{
		sum+=args[i]
	}
 
	return sum
}

func main(){
	res:=sum(1,8,7,5,4,1,34,96);
	fmt.Println(res)

	//匿名函數
	func (n1 int,n2 int) int{
		return n1+n2
	}(10,12)

	//匿名函數
	res2:=func (n1 int,n2 int) int{
		return n1+n2
	}

	fmt.Println(res2(70,50))

	fmt.Println(Fn(55,22))
}

