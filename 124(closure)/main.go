package main

import (
	"fmt"
	"strings"
)

func AddUpper() func(int) int  {
	n:=10
	return func(x int) int{
		n=n+x
		return n
	}
}

func makeSuffix(suffix string) func(string) string{
	return func(name string) string{
		if strings.HasSuffix(name,suffix){
			return name
		}else{
			return name+suffix
		}
	}
}

func main(){
	f:=AddUpper()
	fmt.Println(f(1))
	fmt.Println(f(2))
	fmt.Println(f(3))

	s:=makeSuffix(".jpg")
	fmt.Println(s("ggg.jpg"))

}

