package main

import (
	"fmt"
	"strconv"
	"strings"
)


func main(){
	//字串長度(字母、數字=>1個字節、中文=>3個字節)
	s:="1234567890"
	fmt.Println("字串長度=",len(s))

	//遍歷字串，並處理中文亂碼問題
	s2:="test測試測試"
	s3:=[]rune(s2)//處理中文亂碼問題
	for i:=0;i<len(s3);i++{
		fmt.Printf("字串=%c\n",s3[i])
	}

	//字串轉整數
	n,err:=strconv.Atoi("123測試")
	if err!=nil{
		fmt.Println("轉換錯誤",err)
	}else{
		fmt.Println("字串轉換成整數結果",n)
	}

	//整數轉字串
	n2:=strconv.Itoa(1234)
	fmt.Printf("整數轉換成字串結果%T\n",n2)
	
	//字符串轉byte
	s4:=[]byte("hello word")
	fmt.Printf("字符串轉換成byte結果%v\n",s4)

	//byte轉字符串
	s5:=string([]byte{97,98,99})
	fmt.Printf("轉換成字符串結果%v\n",s5)

	//10進制轉2、8、16
	s6:=strconv.FormatInt(1230,2)//2->8、16
	fmt.Printf("10進制轉換成2進制結果%v\n",s6)
	s7:=strconv.FormatInt(1230,8)
	fmt.Printf("10進制轉換成8進制結果%v\n",s7)

	//查詢字串是否在另一字串內(中文也行)
	b:=strings.Contains("seafood","foo")
	fmt.Printf("是否包含foo字串%v\n",b)

	//查詢字串包含多少個另一字串(中文也行)
	c:=strings.Count("seafood","o")
	fmt.Printf("包含幾個o字串%v\n",c)

	//不區分大小寫的字串比較(==是有區分大小寫)
	b2:=strings.EqualFold("ABC","abc")
	fmt.Printf("字串是否一致%v\n",b2)
	fmt.Println("字串是否一致","ABC"=="abc")

	//查詢字串在另一字串內第一個出現的index
	index:=strings.Index("qwewqeq_abc_eree_abc","abc")
	fmt.Println("index=",index)

	//查詢字串在另一字串內最後一次出現的index
	index2:=strings.LastIndex("qwewqeq_abc_eree_abc","abc")
	fmt.Println("index=",index2)

	//覆蓋字串(n=要替換幾個，-1表示全部)
	str_replace:=strings.Replace("qwewqeq_abc_eree_abc","abc","cde",-1)
	fmt.Println("str_replace=",str_replace)

	//切割字串為數組
	strArr:=strings.Split("a,b,c,d,e,f",",")
	fmt.Println("strArr=",strArr)
	for i := 0; i < len(strArr); i++ {
		fmt.Println("str=",strArr[i])
	}

	//字串大小寫轉換
	lowStr:=strings.ToLower("BSDSQWE")
	fmt.Println("lowStr=",lowStr)

	upStr:=strings.ToUpper("qwefqewrw")
	fmt.Println("upStr=",upStr)

	//字串去除左右空白
	str:=strings.TrimSpace("   abcde   ")
	fmt.Println("str=",str)

	//字串左右去除指定字串
	str=strings.Trim("!ab!cde!","!")
	fmt.Println("str=",str)

	//字串是否以某個字開頭
	b3:=strings.HasPrefix("http://aa.bb.cc","http")
	fmt.Println("結果=",b3)

	//字串是否以某個字結尾
	b4:=strings.HasSuffix("xxxx.jpg","jpg")
	fmt.Println("結果=",b4)

}