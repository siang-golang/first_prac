package main

import (
	"fmt"
	"strconv"
	"time"
)

func test03(){
	str:=""
	for i := 0; i < 50000; i++ {
		str+="hello"+strconv.Itoa(i)
	}
}

func main(){
	
	//當前時間
	now:=time.Now()
	fmt.Printf("time:%v type:%T\n",now,now)

	// start:=time.Now().Unix()
	// test03()
	// end:=time.Now().Unix()
	// total:=end-start
	// fmt.Printf("開始:%v,結束:%v,總計:%v秒\n",start,end,total)

	//獲取now的年月日十分秒
	y:=now.Year()
	m:=int(now.Month())
	d:=now.Day()
	h:=now.Hour()
	i:=now.Minute()
	s:=now.Second()
	fmt.Printf("%v-%v-%v %v:%v:%v\n",y,m,d,h,i,s)
	
	//格式化時間
	str:=fmt.Sprintf("%d-%d-%d %d:%d:%d",y,m,d,h,i,s)
	fmt.Println(str)
	str2:=now.Format("2006/01/02 15:04:05")
	fmt.Println(str2)

	//1.每隔1秒打印一個數字，打到10結束
	//2.每隔0.1秒打印一個數字，打到10結束
	for i := 1; i < 6; i++ {
		fmt.Println(i)
		//time.Sleep(time.Second)
		time.Sleep(time.Millisecond*100)
	}

	unix:=now.Unix()
	fmt.Println(unix)
	unixNan:=now.UnixNano()
	fmt.Println(unixNan)
}