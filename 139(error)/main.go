package main

import (
	"errors"
	"fmt"
)

func test(){
	defer func ()  {
		//內置函數，來捕獲和處理異常
		if err:=recover(); err !=nil{
			fmt.Println("err:",err)
		}
	}()
	n1:=100
	n2:=0
	res:=n1/n2
	fmt.Println("res:",res)
}

func readConf(name string) (err error) {
	if name=="conf.ini"{
		//讀取
		return nil
	}else{
		//自定義錯誤
		return errors.New("檔名無法讀取")
	}
}

func test2(){
	err:=readConf("ccccc")
	if err != nil{
		//如果讀取發生錯誤，就輸出錯誤，並終止程式
		panic(err)
	}
}

func main() {
	//test()
	test2()
	fmt.Println("main")
}


