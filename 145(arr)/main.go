package main

import (
	"fmt"
)


func main() {
	var score [5]float64
	for i:=0;i<len(score);i++{
		//fmt.Printf("請輸入第%d個元素\n",i+1)
		//fmt.Scanln(&score[i])
	} 

	for i:=0;i<len(score);i++{
		fmt.Printf("score[%d]=%v\n",i,&score[i])
	}

	//四種初始化數組的方式
	var numArr01 [3]int  = [3]int{1,2,3}
	fmt.Println("numArr01=",numArr01)

	var numArr02 = [3]int{1,2,3}
	fmt.Println("numArr02=",numArr02)

	var numArr03 = [...]int{1,2,3,4} //[...]是固定寫法
	fmt.Println("numArr03=",numArr03)

	var numArr04 = [...]int{0:4,1:3,2:2,3:1} //[...]是固定寫法
	fmt.Println("numArr04=",numArr04)

	//類型推導
	var numArr05 = [...]string{0:"qwe",1:"dddd",2:"aaaa",3:"vvvv"} //[...]是固定寫法
	fmt.Println("numArr05=",numArr05)


	//for-range
	for i,v :=range numArr05{
		fmt.Println("i=",i)
		fmt.Println("v=",v)
	}
}


