package main

import (
	"fmt"
	"math/rand"
	"time"
)

func test(arr[3]int){
	arr[0]=88
}

func test2(arr *[3]int){
	fmt.Printf("arr的地址是%p",&arr)
	arr[0]=88
}

func main() {
	//數組是多個相同類型數據的組合，一個數組一旦聲明/定義了，其長度就是固定的，不會動態變化
	var arr01 [3]int
	arr01[0]=1
	arr01[1]=78
	arr01[2]=1677
	fmt.Println(arr01)

	//數組創建後，未賦值，會有默認值
	var arr002 [3]int
	var arr003 [3]string
	var arr004 [3]bool
	var arr005 [3]float64

	fmt.Println(arr002)
	fmt.Println(arr003)
	fmt.Println(arr004)
	fmt.Println(arr005)
	
	//數組下標是從0開始
	var arr001 [3]string
	//var index int= 3
	//arr001[index]="eeeeeee"//只能0-2，因此arr001[3]越界
	fmt.Println(arr001)

	arr006 :=[3]int{55,66,77}
	test(arr006)
	test2(&arr006)
	fmt.Println(arr006)

	//建立數組顯示A-Z
	var mychart [26]byte
	for i := 0; i < len(mychart); i++ {
		mychart[i]='A'+byte(i)
	}

	for i := 0; i < len(mychart); i++ {
		fmt.Printf("%c ",mychart[i])
	}
	fmt.Printf("\n")
	//請找出一個數組的最大，並得到其對應下標
	arr:= [...]int{1,-1,100,85,5,7,200}
	
	max:=arr[0]
	index :=0
	for i := 0; i < len(arr); i++ {
		if arr[i]>max{
			max=arr[i]
			index=i
		}
	}
	fmt.Printf("最大值:%v 下標:%v\n",max,index)

	//請取得一個數組的總和跟平均
	arr2:= [...]int{1,-1,100,85,5,7,200}
	sum:=0
	for _,v :=range arr2{
		sum+=v
	}
	average:=float64(sum)/float64(len(arr2))
	fmt.Printf("總和:%v 平均:%v\n",sum,average)

	//隨機產生5個數的數組，並反轉打印
	//隨機生成 rand.Intn
	//為了每次得到不一樣的值，須設定seed值
	var randArr [8]int
	leng :=len(randArr)
	rand.Seed(time.Now().Unix())
	for i := 0; i < leng; i++ {
		randArr[i]=rand.Intn(100)//0<=n<100
	}
	fmt.Println(randArr)
	var returnArr [len(randArr)]int
	k:=0
	for i := leng-1; i >=0; i-- {
		returnArr[k]=randArr[i]
		k++
	}
	fmt.Println(returnArr)

	
}	

