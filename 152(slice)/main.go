package main

import(
	"fmt"
)


func main(){
	//切片的基本使用方式一
	var intArr [5]int= [...]int{4,8,-5,97,45}
	//slice := intArr[1:3] 聲明或定義一個切片
	//1. slice 是切片名
	//2. intArr[1:3] 表示slice 引用到intArr這個數組
	//3. intArr數組的索引值1到3但不包含3
	slice := intArr[1:3] 
	fmt.Println("intArr=",intArr)
	fmt.Printf("intArr地址%p\n",&intArr)
	fmt.Println("slice的元素是",slice)
	fmt.Println("slice的元素個數",len(slice))
	fmt.Println("slice的容量",cap(slice)) //切片的容量是可以動態變化的
	fmt.Printf("intArr[1]地址%p\n",&intArr[1])
	fmt.Printf("slice[0]地址%p\n",&slice[0])

	//方式二
	var slice2 []float64 = make([]float64, 5 ,15)
	
	slice2[0] = 25
	slice2[1] = 88.88
	fmt.Println(slice2)
	fmt.Println("slice2的元素個數",len(slice2))
	fmt.Println("slice2的容量",cap(slice2)) //切片的容量是可以動態變化的


	//方式三
	var slice3 []float64 = []float64{47,85,14,16,20}
	fmt.Println(slice3)
	fmt.Println("slice3的元素個數",len(slice3))
	fmt.Println("slice3的容量",cap(slice3)) //切片的容量是可以動態變化的

}