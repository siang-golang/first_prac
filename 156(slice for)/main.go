package main

import(
	"fmt"
)


func main(){
	//for slice
	var arr [5]int = [...]int{10,20,30,40,50}
	slice := arr[1:4] //20,30,40
	for i := 0; i < len(slice); i++ {
		fmt.Printf("slice[%v]=%v ",i,slice[i])
	}

	fmt.Println()

	//for range slice
	for i,v :=range slice{
		fmt.Printf("slice[%v]=%v ",i,v)
	} 
	fmt.Println()

	slice2 := arr[:] 
	fmt.Println("slice2=",slice2)
	slice3 := arr[2:] 
	fmt.Println("slice3=",slice3)
	slice4 := arr[:3] 
	fmt.Println("slice4=",slice4)


	//append內建函數，可以對切片動態新增
	var slice5 []int = []int{}	
	fmt.Println("slice5=",slice5)
	
	//用append追加
	slice5 = append(slice5,100)
	slice5 = append(slice5,200)
	slice5 = append(slice5,300,400,500)
	fmt.Println("slice5=",slice5)

	slice5 = append(slice5,slice5...)
	fmt.Println("slice5=",slice5)

	//切片拷貝
	var slice6 []int = []int{100,200,300}
	var slice7 = make([]int,10)
	copy(slice7,slice6)	
	fmt.Println("slice7=",slice7)
	
	

}