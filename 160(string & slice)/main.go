package main

import(
	"fmt"
)

func main(){
	str:="hello@atguigui"
	slice:=str[6:]
	fmt.Println(slice)
	
	//string 是不可變的，因此不能直接使用str[0]="z" 來修改字符串，會報錯
	//如果要修改字符串，先將 string ->[]byte  / []rune ->修改 ->重新轉成string

	//將hello@atguigui => 改成zello@atguigui
	arr1:= []byte(str)
	arr1[0]='z'
	str=string(arr1)
	fmt.Println(str)

	//細節，我們轉成[]byte後，可以處理英文和數字，但中文無法處理
	//因為[]byte是字節處理，而一個漢字是3個字節，因此會出現亂碼
	//解決方法將string 轉成[]rune 即可，因為rune是按字符處理，可兼容漢字
	arr2:= []rune(str)
	arr2[0]='北'
	str=string(arr2)
	fmt.Println(str)

}