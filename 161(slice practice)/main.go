package main

import(
	"fmt"
)

func fbn(n int) ([]uint64){
	//聲明一個切片
	fbSlice :=make([]uint64,n) 
	fbSlice[0]=1
	fbSlice[1]=1

	for i := 2; i < n; i++ {
		fbSlice[i]=fbSlice[i-1]+fbSlice[i-2]
	}

	return fbSlice
}

func main(){
	/*
		1.可以接收 n int
		2.可以將婓波那契的數列放到切片中
		3.婓波那契的數列形式
		arr[0]=1 , arr[1]=1, arr[2]=2, arr[3]=3, arr[4]=5, arr[5]=8
	*/
	fbSlice := fbn(10)
	fmt.Println(fbSlice)
}