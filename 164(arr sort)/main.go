package main

import(
	"fmt"
)

//冒泡排序
func BubbleSort(arr *[6]int){
	fmt.Println("排序前arr",(*arr))
	
	tmp :=0//臨時變量(用於交換用)
	for i := 0; i < len(*arr)-1; i++ {
		flag := 0  
		for j := 0; j < len(*arr)-1-i; j++ {
			if (*arr)[j] > (*arr)[j+1]{
				//交換
				tmp =(*arr)[j]
				(*arr)[j]=(*arr)[j+1]
				(*arr)[j+1]=tmp
				flag=1
			}
		}

		if flag==0 {
			break
		}

	}
}


//二分查找
func BinaryFind(arr *[6] int, leftondex int , rightindex int , findVal int){
	if leftondex > rightindex{
		fmt.Printf("%v不在數組內\n", findVal)  
		return
	}
	//先找中間下標
	mid := (leftondex + rightindex)/2
	if (*arr)[mid] > findVal {
		//如果arr[mid] > findVal  =>  向leftindex-----mid-1
		BinaryFind(arr, leftondex , mid-1 , findVal)
	} else if (*arr)[mid] < findVal {
		//如果arr[mid] < findVal  =>  向mid+1-----rightindex
		BinaryFind(arr, mid+1 , rightindex , findVal)
	}else {
		fmt.Printf("找到了 下標為%v\n",mid)  
	}
}

func main(){
	arr := [6]int{77,51,22,34,14,28}
	fmt.Println(arr)

	BubbleSort(&arr)
	fmt.Println(arr)

	name := []string{"金毛獅王","白眉鷹王","紫杉龍王","青翼蝠王"} 
	var heroname = ""
	fmt.Println("請輸入要查找的人")
	fmt.Scanln(&heroname)

	//順序查找，方法1
	for i := 0; i < len(name); i++ {
		if heroname == name[i]{
			fmt.Printf("找到%v,下標%v\n",heroname,i)
			break
		}else if i == len(name)-1 {	
			fmt.Printf("沒有找到%v\n",heroname)
		}
	}

	//順序查找，方法2
	index := -1
	for i := 0; i < len(name); i++ {
		if heroname == name[i]{
			index = i
			break
		}
	}

	if index != -1{
		fmt.Printf("找到%v,下標%v\n",heroname,index)
	}else{
		fmt.Printf("沒有找到%v\n",heroname)
	}

	//對一個有序數組進行二分查找，輸入一個數看是否在數組內
	numArr :=[6] int{1,8,9,89,1000,1234}
	/*
	思路 要查的是 findVal
	1.數組必須為有序數組，從小到大排序
	2.先找到中間下標mid=(leftindex+rightindex)/2，然後讓中間下標得值跟findVal比較
	2.1 如果arr[mid] > findVal  =>  向leftindex-----mid-1
	2.2 如果arr[mid] < findVal  =>  向mid+1-----rightindex
	2.3 如果arr[mid] == findVal  => 表示找到
	3. 會使用上方2的邏輯遞規執行
	*/

	findVal := 0
	fmt.Println("請輸入要查找的數")
	fmt.Scanln(&findVal)
	BinaryFind(&numArr , 0 , len(numArr)-1 , findVal)


}