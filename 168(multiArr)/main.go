package main

import (
	"fmt"
)


func main() {
	//二維數組
	/*
	0 0 0 0 0 0
	0 0 1 0 0 0
	0 2 0 3 0 0
	0 0 0 0 0 0
	*/
	var arr  [4][6] int
	arr[1][2]=1
	arr[2][1]=2
	arr[2][3]=3
	for i:=0;i<len(arr);i++{
		for j:=0;j<len(arr[i]);j++{
			fmt.Print(arr[i][j], " ")
		}
		fmt.Println()
	}

	//用for range的寫法
	for _, v := range arr {
		for _, v2 := range v {
			fmt.Print(v2, " ")
		}
		fmt.Println()
	}

	//聲明/定義的不同寫法
	var arr2  [2][3] int= [2][3] int {{1,2,3},{3,2,1}}
	var arr3  [2][3] int= [...][3] int {{1,2,3},{3,2,1}}
	var arr4  =  [2][3] int {{1,2,3},{3,2,1}}
	var arr5  =  [...][3] int {{1,2,3},{3,2,1}}
	fmt.Println(arr2)
	fmt.Println(arr3)
	fmt.Println(arr4)
	fmt.Println(arr5)

	var score [3][5] int
	for i:=0;i<len(score);i++{
		for j:=0;j<len(score[i]);j++{
			fmt.Printf("請輸入第%v班的第%v個學生\n", i+1, j+1)
			fmt.Scanln(&score [i][j])
		}
	}

	fmt.Println(score)

	total :=0
	for i:=0;i<len(score);i++{
		sum := 0
		for j:=0;j<len(score[i]);j++{
			total+=score[i][j]
		}
		total+=sum
		var avg =sum/len(score[i])
		fmt.Printf("第%v班平均%v\n",i+1,avg)
	}
	
	fmt.Printf("所有班平均%v\n",total/15)
}


