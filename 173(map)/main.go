package main

import (
	"fmt"
)


func main() {
	//map的聲明跟注意事項

	//方法一
	var a map[string]string //先聲明，此時的map是nil
	//在使用map前需先make，make的作用就是分配數據空間
	a = make(map[string]string, 10)
	a["no1"] = "第一個"
	a["no2"] = "第二個"
	a["no1"] = "蓋掉第一個"
	fmt.Println(a)

	//方法二，聲明時就直接make
	var b = make(map[string]string)
	b["no1"] = "第一個"
	b["no2"] = "第二個"
	fmt.Println(b)

	//方法三，聲明直接賦值
	var c  map[string]string = map[string]string {
		"no1" : "第一個",
		"no2" : "第二個",
	} 
	/*
		c := map[string]string {
			"no1" : "第一個",
			"no2" : "第二個",
		} 
	*/
	fmt.Println(c)
	
	d := make(map[string]map[string]string)
	d["stu1"] =make(map[string]string ,2)
	d["stu1"]["name"]="e1"
	d["stu1"]["sex"]="F"
	d["stu2"] =make(map[string]string ,2)
	d["stu2"]["name"]="e2"
	d["stu2"]["sex"]="M"
	d["stu3"] =make(map[string]string ,2)
	d["stu3"]["name"]="e3"
	d["stu3"]["sex"]="F"
	fmt.Println(d)


	//map新增跟修改
	e := make(map[string]string, 10) 
	e["no1"] = "1" //如果key沒有就自動新增
	e["no2"] = "2"	
	fmt.Println(e)
	e["no1"] = "3" //如果key存在就自動修改該key的value
	e["no3"] = "4"
	fmt.Println(e)

	//map的刪除
	delete(e , "no1") //key =>no1 存在就刪除，不存在也不會報錯
	fmt.Println(e)
	delete(e , "no15") //key =>no1 存在就刪除，不存在也不會報錯
	fmt.Println(e)

	/*
		map如要刪除全部的key
		1. 遍歷所有的key
		2. 直接make一個新空間	
	*/

	//2.
	e = make(map[string]string) 
	fmt.Println(e)

	//map的查找
	f := make(map[string]string, 10) 
	f["no1"] = "測試1" 
	f["no2"] = "測試2"	

	val, findRes := f["no1"]
	if findRes {
		fmt.Printf("找到該key的值為%v\n", val)
	} else{
		fmt.Printf("找不到\n")
	}
	
	//map的遍歷只能用for-range
	for k, v := range f {
		fmt.Printf("k=%v,v=%v\n",k,v)
	}

	for k, v := range d {
		fmt.Printf("k=%v\n",k)
		for i, val := range v {
			fmt.Printf("\t i=%v,val=%v\n",i,val)
		}
	}

	//取得map長度
	fmt.Println("長度為",len(d))
}


