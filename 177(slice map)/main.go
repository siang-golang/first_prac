package main

import (
	"fmt"
	"sort"
)

func modifyUser(user map[string]map[string]string, name string){

	if user[name] != nil{
		user[name]["pwd"]="8888"
	}else{
		newStu :=make(map[string]string)
		newStu["nickname"]="暱稱|"+name
		newStu["pwd"]="0000"
		user[name]=newStu
	}

}

func modifyMap(map2 map[int]int){
	map2[0]=900
}

//定義一個學生結構體
type Stu struct{
	Name string
	Age int
	Add string
}

func main() {
	//map的切片使用
	/*
		要求:用map來記錄monster的name跟age，也就是說monster對應一個map
		，並且妖怪的個數是可以動態新增的 => 動態切片
	*/

	//1.聲明一個map切片
	monsters := make([]map[string]string,2) //準備放入2個妖怪
	//增加第一個妖怪訊息
	if monsters[0] == nil {
		monsters[0] = make(map[string]string, 2)
		monsters[0]["name"]="牛魔王"
		monsters[0]["age"]="200"
	}

	if monsters[1] == nil {
		monsters[1] = make(map[string]string, 2)
		monsters[1]["name"]="孫悟空"
		monsters[1]["age"]="100"
	}

	/*
		//下方寫法越界，因為只有2個
		if monsters[2] == nil {
			monsters[2] = make(map[string]string, 2)
			monsters[2]["name"]="豬八戒"
			monsters[2]["age"]="300"
		}
	*/
	
	//我們使用切片的append函數，動態新增monsters
	newMaster := map[string]string{
		"name": "豬八戒",
		"age" : "300",
	}

	monsters =append( monsters ,newMaster)
	fmt.Println(monsters)

	//map排序
	/*
		1.golang沒有方法可以針對map的key進行排序
		2.golang中的map默認是無序的，注意也不是照添加順序存放的
		因此每次遍歷，得到的結果可能不一樣
		3.golang中map的排序，是先將key進行排序，然後根據key值遍歷輸出即可
	*/
	map1 := make(map[int]int , 10)
	map1[0]=10
	map1[10]=100
	map1[4]=40
	map1[8]=80
	fmt.Println(map1)
	for k, v := range map1 {
		fmt.Printf("k=%v v=%v\n",k,v)
	}
	
	//如果要按照map的key的順序進行排序輸出
	/*
		1. 先將map的key放到切片中
		2. 對切片排序
		3. 遍歷切片，然後按照key來輸出map的值
	*/
	//1.
	var keys []int
	for k, _ := range map1 {
		keys= append(keys,k)
	}
	//2.
	sort.Ints(keys)
	fmt.Println(keys)

	//3.
	for _, k := range keys {
		fmt.Printf("map1[%v]=%v\n",k,map1[k])
	}

	/*
	* 	map的使用細節
	*	1. map是引用類型，遵守引用類型傳遞的機制，在一個函數接收map後，map修改後會直接修改原來的map
	*	2. map 能動態增長鍵值
	*	3. map的value經常用到struct類型，更適合管理複雜的數據
	*/
	map2 := make(map[int]int,2) //設定為2
	map2[0]= 200
	map2[1]= 900
	map2[2]= 500 //超過還是能自動動態增長(2)
	map2[10]= 100 //超過還是能自動動態增長(2)
	map2[15]= 1500 //超過還是能自動動態增長(2)

	fmt.Println(map2)
	modifyMap(map2)
	//結果 map2[0]= 900，表示map是引用類型(1)
	fmt.Println(map2)

	//(3)
	//1. map的key 為學生的學號，是唯一的
	//2. map的value 是struct結構體，包含學生的姓名、年齡、地址
	stud := make(map[string]Stu, 10)
	//創建兩個學生
	//stu1 := Stu{"Eric1",15,"taipei"}
	//stu2 := Stu{"Eric2",12,"tainan"}
	stud["001"]=Stu{"Eric1",15,"taipei"}
	stud["002"]=Stu{"Eric2",12,"tainan"}

	fmt.Println(stud)

	for k, v := range stud {
		fmt.Printf("學號:%v\n",k)
		fmt.Printf("學生名字:%v\n",v.Name)
		fmt.Printf("學生年齡:%v\n",v.Age)
		fmt.Printf("學生地址:%v\n",v.Add)
	}

	/*
	*	課程練習 
	*	1. 使用map[string]map[string]string 類型
	*	2. key 是用戶名，唯一的不可重複
	*	3. 如果用戶存在，就修改密碼為8888，不存在就新增用戶訊息(nickname、pwd)
	*	4. 編寫函式 modifyUser(map[string]map[string]string, name string)完成此功能
	*/
	user :=make(map[string]map[string]string)
	
	modifyUser(user, "測試")
	modifyUser(user, "gaqqqwq")
	modifyUser(user, "測試")
	fmt.Println(user)

}


