package main

import (
	"fmt"
)

//定義一個Cat結構體，將Cat的各字段/屬性訊息，放入到Cat結構體進行管理
type Cat struct{
	Name string
	Age int
	Color string
}

//如果結構體的字段/屬性是slice、map的預設值都是nil，表示還沒分配空間
//如要使用需先make才能使用
type Persion struct{
	Name string
	Age int
	Score [3]int //數組
	ptr *int //指針
	slice []int //切片
	map1 map[string]string //map
}

type Monster struct{
	Name string
	Age int
	
}

func main() {
	//使用struct
	//創建一個Cat變量
	var cat1 Cat
	fmt.Printf("cat1的地址:%p\n", &cat1)
	cat1.Name="小花"
	cat1.Age=2
	cat1.Color="麻花色"
	fmt.Println(cat1)
	fmt.Println("貓名:",cat1.Name)
	fmt.Println("貓年齡:",cat1.Age)
	fmt.Println("貓色:",cat1.Color)

	var p1 Persion
	fmt.Println(p1)
	if p1.ptr==nil{
		fmt.Println("ok1")
	}

	if p1.slice==nil{
		fmt.Println("ok2")
	}

	if p1.map1==nil{
		fmt.Println("ok3")
	}

	p1.slice = make([]int,10)
	p1.slice[0] = 5
	if p1.slice == nil{
		fmt.Println("exist")
	}
	fmt.Println(p1)

	p1.map1 = make(map[string]string)
	p1.map1["eric1"] = "eric1" 
	fmt.Println(p1)

	var m1 Monster
	m1.Name="牛魔王"
	m1.Age=50
	fmt.Printf("m1的地址:%p\n", &m1)
	fmt.Println(m1)
	m2 := m1 //結構體默認值拷貝
	m2.Name = "孫悟空"
	fmt.Printf("m2的地址:%p\n", &m2)
	fmt.Println(m2)

	//創建結構體的聲明方法
	//方法1
	var cat2 Cat
	//方法2
	var cat3 Cat = Cat{"小白",3,"白色"}
	cat4  := Cat{"小橘",4,"橘色"}
	//方法3
	var cat5 *Cat = new(Cat)
	//因為cat5 是指針，因此給值方式為
	//(*cat5).Name="小黑"  也可以這樣寫 cat5.Name="小黑"
	//原因  因為go設計者，為了程式員方便底層會對cat5.Name="小黑" 進行處理
	//會給cat5加上取值運算 (*cat5).Name="小黑"
	(*cat5).Name="小黑"
	cat5.Name="黑黑黑"
	//方法4，也是指針，賦值方式跟上方一樣
	var cat7 *Cat = &Cat{}
	var cat6 *Cat = &Cat{"阿灰",5,"灰色"}
	(*cat6).Name="小灰"
	cat6.Name="小灰灰"
	fmt.Println(cat2,cat3,cat4,*cat5,*cat6,*cat7)
	fmt.Printf("%p\n",&cat5)
	fmt.Printf("%p\n",&cat5.Name)
}


