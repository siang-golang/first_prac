package main

import (
	"encoding/json"
	"fmt"
)

type Point struct {
	x int
	y int
}

type Rect struct{
	leftUp,rightDown Point
}

type Rect2 struct{
	leftUp,rightDown *Point
}

type B struct{
	Num int
}

type C struct{
	Num int

}

type Monster struct{
	Name string `json:"name"` //結構體的標籤 struct tag
	Age int `json:"age"`
	Skill string `json:"skill"`
}


func main() {
	//r1 有4個int ,在內存中是連續的
	r1 := Rect{Point{1,2},Point{3,4}}

	fmt.Printf("r1.leftUp.x 地址=%p \nr1.leftUp.y 地址=%p \nr1.rightDown.x  地址=%p \nr1.rightDown.y 地址%p\n",&r1.leftUp.x,&r1.leftUp.y,&r1.rightDown.x,&r1.rightDown.y)

	//r2 有兩個 *Point類型，這兩個*Point類型(指針類型)的本身地址也是連續的，但他們指向的地址不一定是連續的
	r2 :=Rect2{&Point{5,6},&Point{7,8}}

	//本身地址是連續的
	fmt.Printf("r2.leftUp 本身地址=%p\nr2.rightDown 本身地址=%p \n",&r2.leftUp,&r2.rightDown)

	//指向的地址不一定是連續的... 這要看運行時是如何分配的，因此可能連續、也可能不連續
	fmt.Printf("r2.leftUp 指向地址=%p\nr2.rightDown 指向地址=%p \n",r2.leftUp,r2.rightDown)

	var b B
	var c C
	//b=c 會報錯，不能直接將b只給c
	b = B(c) //=>結構體的字段要完全相同，才能強制轉  (名字、個數、類型都樣完全一樣)
	fmt.Println(b,c)

	//struct tag(反射機制用) 範例
	//創建一個monster變量
	var monster  = Monster{"牛魔王",800,"鼻環攻擊"}
	//將monster變量序列化為 json格式
	//json.Marshal 函數中使用反射，因此可以解析struct tag，反射後面再講
	jsonStr, err := json.Marshal(monster)
	if err != nil{
		fmt.Println("發生錯誤:",err)
	}

	fmt.Println(jsonStr)
	fmt.Println(string(jsonStr))
}


