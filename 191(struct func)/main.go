package main

import (
	"fmt"
)

type Person struct{
	Name string
}

//給Person類型綁定方法test
func (person Person) test(){
	person.Name="ggggg"
	fmt.Println("test() name=",person.Name)
}

//給Person結構體添加speak 方法 ，輸出xxx是好人
func (person Person) speak(){
	fmt.Printf("%v是好人\n",person.Name)
}

func (person Person) jisuan(){
    res :=0
	for i := 1; i <= 1000; i++ {
		res+= i
	}
	fmt.Println("計算結果是",res)
}

func (person Person) jisuan2(n int){
    res :=0
	for i := 1; i <= n; i++ {
		res+= i
	}
	fmt.Println("計算結果是",res)
}

func (person Person) getSum(n1 int ,n2 int) int{
    return n1 + n2
}

type Dog struct{

}

type Circle struct{
	radius float64
}

func (c Circle) area() float64{
	c.radius=5
	return c.radius * c.radius * 3.14 
}

//為了提高效率，通常方法會和結構體的指針類型綁定
func (c *Circle) area2() float64{
	//因為c是指針，因此我們標準訪問的字段是 (*c).radius
	//return (*c).radius * (*c).radius * 3.14 
	//但go底層會自動優化，因此(*c).radius 等價於c.radius
	fmt.Printf("area2 c的地址=%p\n",c)
	c.radius=5
	return c.radius * c.radius * 3.14 
}


func main() {
	var p Person
	p.Name = "Eric"
	p.test();
	//test()  會報錯，不能直接調用
	//var d Dog
	//d.test() //不是Person類型的struct 也是會報錯
	fmt.Println("main name=",p.Name)
	p.speak();
	p.jisuan();
	p.jisuan2(100);
	res := p.getSum(100,50);
	fmt.Println("計算加總結果是",res)


	//申明一個結構體circle ，字段為 radius
	//申明方法area和circle綁定，可以返回面積
	var c Circle
	fmt.Printf("main c的地址=%p\n",&c)
	c.radius = 2.5	
	fmt.Println("圓形面積=",c.area())
	fmt.Println("c.radius=",c.radius)
	//res2 := (&c).area2()
	//go底層會自動優化，(&c).area2() 等價於c.area2()
	//會自動將加上&c
	res2 := c.area2()
	fmt.Println("圓形面積=",res2)
	//因為是指針類型，內存是同樣的，因此在方法內修改radius，外部調用也會一起被修改
	fmt.Println("c.radius=",c.radius)

}


