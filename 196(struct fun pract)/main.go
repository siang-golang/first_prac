package main

import (
	"fmt"
)

type MethodUtils struct{

}

func (mu *MethodUtils) Print(){
	for i := 1; i <= 10; i++ {
		for j := 1; j <= 8; j++ {
			fmt.Print("*")
		}
		fmt.Println()
	}
}

func (mu *MethodUtils) Print2(m int, n int){
	for i := 1; i <= m; i++ {
		for j := 1; j <= n; j++ {
			fmt.Print("*")
		}
		fmt.Println()
	}
}

func (mu *MethodUtils) area(len float64, wid float64) float64 {
	return  len * wid
}

func (mu *MethodUtils) Print3(m int, n int , printStr string){
	for i := 1; i <= m; i++ {
		for j := 1; j <= n; j++ {
			fmt.Print(printStr)
		}
		fmt.Println()
	}
}

func (mu *MethodUtils) judgeNum(num int)  {
	if num % 2 ==0 {
		fmt.Println("是偶數")
	}else{
		fmt.Println("是奇數")
	}
}

func main() {
	var mu MethodUtils
	mu.Print()
	mu.Print2(4,5)

	res := mu.area(5,20)
	fmt.Println("面積",res)
	mu.judgeNum(5)

	mu.Print3(4,5,"/")
}


