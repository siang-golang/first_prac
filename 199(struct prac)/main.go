package main

import (
	"fmt"
)

//學生的訊息
type Student struct{
	name string
	gender string
	age int
	id int
	score float64
}

func (stu *Student) say() string{
	res := fmt.Sprintf("姓名:%v 性別:%v  年齡:%v  學號:%v  分數:%v",stu.name,stu.gender,stu.age,stu.id,stu.score)
	return res
}

//狗的訊息
type Dog struct{
	name string
	age  int
	weight float64
}

func (d *Dog) say() string{
	res := fmt.Sprintf("狗名:%v 年齡:%v  體重:%v  ",d.name,d.age,d.weight)
	return res
}

//長方體訊息
type Box struct{
	length  float64
	width 	float64
	height 	float64
}

//體積計算
func (b *Box) volume() float64{
	return b.length * b.width * b.height
}

//參訪者訊息
type Vistor struct{
	name string
	age  int
}

func (v *Vistor) getTicket() int{
	if v.age > 18 {
		return 20
	}else{
		return 0;
	}
}

type Test struct{
	name string
	age  int
}


func main() {

	stu := Student{
		name:"Eric",
		gender:"M",
		age:32,
		id:880,
		score:50,
	}

	res :=stu.say()
	fmt.Println(res)

	d := Dog{"大大",13,20}
	dres :=d.say()
	fmt.Println(dres)

	b := Box{12,52,86}
	bres :=b.volume()
	fmt.Println("體積",bres)

	var v Vistor
	for{
		fmt.Println("請輸入姓名")
		fmt.Scanln(&v.name)
		if v.name == "n"{
			fmt.Println("退出程序")
			break;
		}
		fmt.Println("請輸入年齡")
		fmt.Scanln(&v.age)
		fmt.Println("票價為")
		fmt.Println(v.getTicket())
	}

	//字段聲明的方法
	//方法一
	var t1 Test = Test{"tom",10}
	t2 := Test{"tom",10}
	var t3 Test = Test{
		name:"tom",
		age:10,
	}
	t4 := Test{
		name:"tom",
		age:10,
	}

	//方法二
	var t5  = &Test{"tom",10}
	var t6  = &Test{
		name:"tom",
		age:10,
	}

	fmt.Println(t1,t2,t3,t4,*t5,*t6)
}


