package main

import (
	"fmt"
)
var n1 int =   5


func main() {
	fmt.Printf("其他類型轉字串\n")
	var n1 int = 10
	var n2 float64 = 123.456   
	var b bool = false
	var myChar byte =  'a'
	var str string

	str = fmt.Sprintf("%d",n1)
	fmt.Printf("str type= %T str = %q\n",str,str)

	str = fmt.Sprintf("%f",n2)
	fmt.Printf("str type= %T str = %q\n",str,str)

	str = fmt.Sprintf("%t",b)
	fmt.Printf("str type= %T str = %q\n",str,str)

	str = fmt.Sprintf("%c",myChar)
	fmt.Printf("str type= %T str = %q\n",str,str)
}


