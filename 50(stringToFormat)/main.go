package main

import(
	"fmt"
	"strconv"
)

func main() {
	fmt.Printf("字串轉其他類型\n")
	var str string = "true"
	var b bool
	b , _ = strconv.ParseBool(str)
	fmt.Printf("type= %T str=%v\n",b,b)

	var str2 string = "1234"
	var n1 int64
	n1 , _ = strconv.ParseInt(str2,10,64)
	fmt.Printf("type= %T str=%v",n1,n1)
}