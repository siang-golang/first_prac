package main

import(
	"fmt"
)

func main(){
	fmt.Printf("指針\n")
	//基本数据类型在内存布局
	var i int = 10
	// i 的地址是什么,&i
	fmt.Println("i的地址=", &i)

	//1. ptr 是一个指针变量
	//2. ptr 的类型 *int
	//3. ptr 本身的值&i
	var ptr *int =&i
	fmt.Printf("ptr=%v\n", ptr)
	fmt.Printf("ptr 地址=%v\n",&ptr)
	fmt.Printf("ptr 指向的值=%v\n",*ptr)

	fmt.Println("------------------")
	var num int 
	fmt.Printf("num=%v\n", num)
	fmt.Printf("num地址=%v\n", &num)
	var ptr2 *int =&num
	*ptr2 = 20
	fmt.Printf("num 指向的值=%v\n",num)
}