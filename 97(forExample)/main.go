package main

import(
	"fmt"
)

func main(){
	var end int =5
	for i:=1; i<=end; i++{
		for k:=1; k<=end-i; k++{
			fmt.Print(" ")
		}

		for j:=1; j<=2*i-1; j++{
			if j==1 || j==2*i-1 ||  i==end {
				fmt.Print("*")
			}else{
				fmt.Print(" ")
			}
		}
		fmt.Println()
	}

	for i:=1; i<=9; i++{
		for j:=1; j<=i; j++{
			fmt.Printf("%v * %v = %v \t",j,i,j*i)
		}
		fmt.Println()
	}

}